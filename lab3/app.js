var http = require('http');
var fs = require('fs');

var server = http.createServer();

var SITE_ROOT = '/home/lee/cs100/www';

var map = [ [0, 0, 0, 0, 0],
            [0, 1, 1, 0, 2],
            [1, 0, 1, 0, 1],
            [1, 1, 1, 0, 1],
            [0, 1, 1, 1, 1] ];


function init_map () {
  map = [];
  for (var r=0; r<5; r++) {
    var row = [];
    for (var c=0; c<5; c++) {
      row.push(Math.floor(Math.random() * 2));
    }
    map.push(row);
  }
}

server.listen(18888); // use the last 4 digits of your ID preceded by a 1

server.on('request', function (req, res) {
  if (req.url === '/map') {
    res.writeHead(200, { 'Content-type': 'text/json' });
    res.end(JSON.stringify(map));
    return;
  } else if (req.url === '/rmap') {
    init_map();
    res.writeHead(200, { 'Content-type': 'text/json' });
    res.end(JSON.stringify(map));
    return;
  }
  res.end(fs.readFileSync(SITE_ROOT + req.url));
});
